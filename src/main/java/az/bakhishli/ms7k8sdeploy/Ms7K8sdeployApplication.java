package az.bakhishli.ms7k8sdeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms7K8sdeployApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms7K8sdeployApplication.class, args);
	}

}
